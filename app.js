// Task 1 Javascript 101

// 1.
function getTwoArguments(value1, value2) {
    return value1 * value2;
}

// 2.
function getThreeArguments(value1, value2, value3) {
    return value1 + value2 + value3;
}

// 3.
function getOneArgument(value1) {
    return value1 / 2;
}

// 4.
function getArguments(value1, value2, value3) {
    return (value1 + value2) * value3;
}

// Task 1 
// - Illustrate using code how the block scoped varibles work:
    - let and const variable is, where it is declared, block-scoped and it only exists within the current block. It is also destroyed when
      the code block has been executed.

      if (true) {
          let myVar = 150;
          const myConst = 'Hello';
      }

// - Illustrate using code how Hoisting works:
    - Hoisting is a JavaScript mechanism where variables and function declarations are moved to the top of their scope 
      (local scope if declared inside a function or global scope if declared outside of a function) before code execution
       regardless of where the actual declaration has been made.
  
// - In code give an ex of Coercion using different data types:
    2 + '2'
    // 2 coerces to '2'
    // '22'

    2 - '2'
    // '2' coerces to 2
    // 0

    1 + true
    // true coerces to 1
    // 2

    1 + null
    // null coerces to 0
    // 1

    1 + undefined
    // undefined coerces to 0
    // 1

    1 + false
    // false coerces to 0
    // 1

    'abc' + undefined
    // undefined coerces to 'undefined'
    // 'abcundefined'


// Task 1 continued
    // Employee function

function Person(empnumber, name, surname, email) {
    this.empnumber = empnumber;
    this.name = name;
    this.surname = surname;
    this.email = email;
    this.fullName = function() {
        return this.name + ' ' + this.surname;
    }
    this.getEmployeeCard = function() {
        return {
            id: this.empnumber,
            name: this.name,
            surname: this.surname,
            fullname: this.fullName()
        }
    }
}

const berra = new Person(10, 'Berra', 'Jäderlund', 'example@example.com');

    // Project Object  
const project = {
    name: 'FEdev',
    description: 'FE re-skilling project',
    startDate: '2020-02-03',
    endDate: '2020-04-24',
} 